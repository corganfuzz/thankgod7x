class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :order_id
      t.integer :user_id
      t.datetime :appointmentdate

      t.timestamps
    end
  end
end
