class Appointment < ActiveRecord::Base
  belongs_to :order, foreign_key: :order_id
  belongs_to :user
end
